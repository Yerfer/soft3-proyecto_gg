--Elimnar Anteriores--
--Comentar para ejecutar por primeravez, luego descomentar. --
drop table "SOFT3"."PROGRAMA" cascade constraints PURGE;
drop table "SOFT3"."PROFESOR" cascade constraints PURGE;
drop table "SOFT3"."ESTUDIANTE" cascade constraints PURGE;
drop table "SOFT3"."CURSO" cascade constraints PURGE;
drop table "SOFT3"."INSCRIPCION" cascade constraints PURGE;
drop table "SOFT3"."EVALUACION" cascade constraints PURGE;

--Tablas--
create table PROGRAMA(
    codigo number not null,
    nombre varchar2(30) not null,
	primary key (codigo)
);

COMMENT ON TABLE PROGRAMA IS 'Tabla de Programas Academicos.';
COMMENT ON COLUMN PROGRAMA.codigo IS 'Codigo del programa de tamano variable de 1 a 22 bytes, transparente al usuario, se usa para la respectiva identificacion del Programa, se autoincrementa para cada insercion garantizando valores unicos.';
COMMENT ON COLUMN PROGRAMA.nombre IS 'Nombre del programa.';

create table PROFESOR(
	codigo number not null,
	identificacion number not null,
	nombre_1 varchar2(20) not null,
	nombre_2 varchar2(20),
	apellido_1 varchar2(20) not null,
	apellido_2 varchar2(20),
	titulo varchar2(40) not null,
	programa number not null,
	primary key (codigo),
	foreign key (programa) references PROGRAMA(codigo)
);

COMMENT ON TABLE PROFESOR IS 'Tabla de Profesores registrados en la aplicacion.';
COMMENT ON COLUMN PROFESOR.codigo IS 'Codigo del profesor de tamano variable de 1 a 22 bytes, transparente al usuario, se usa para la respectiva identificacion del profesor, se autoincrementa para cada insercion garantizando valores unicos.';
COMMENT ON COLUMN PROFESOR.identificacion IS 'Numero de Identificacion Personal del profesor.';
COMMENT ON COLUMN PROFESOR.nombre_1 IS 'Primer Nombre del Profesor.';
COMMENT ON COLUMN PROFESOR.nombre_2 IS 'Segundo nombre del Profesor, puede ser null.';
COMMENT ON COLUMN PROFESOR.apellido_1 IS 'Primer Apellido del Profesor.';
COMMENT ON COLUMN PROFESOR.apellido_2 IS 'Segundo apellido del Profesor, puede ser null.';
COMMENT ON COLUMN PROFESOR.titulo IS 'Titulo profesional obtenido por el profesor.';
COMMENT ON COLUMN PROFESOR.programa IS 'Codigo de tamano variable de 1 a 22 bytes del programa al que pertenece el profesor o esta contratado.';

create table ESTUDIANTE(
	codigo number not null,
	identificacion number not null,
	nombre_1 varchar2(20) not null,
	nombre_2 varchar2(20),
	apellido_1 varchar2(20) not null,
	apellido_2 varchar2(20),
	programa number not null,
	primary key (codigo),
	foreign key (programa) references PROGRAMA(codigo)
);

COMMENT ON TABLE ESTUDIANTE IS 'Tabla de Estudiantes registrados en la aplicacion.';
COMMENT ON COLUMN ESTUDIANTE.codigo IS 'Codigo del Estudiante de tamano variable de 1 a 22 bytes, transparente al usuario, se usa para la respectiva identificacion del Estudiante, se autoincrementa para cada insercion garantizando valores unicos.';
COMMENT ON COLUMN ESTUDIANTE.identificacion IS 'Numero de Identificacion Personal del Estudiante.';
COMMENT ON COLUMN ESTUDIANTE.nombre_1 IS 'Primer Nombre del Estudiante.';
COMMENT ON COLUMN ESTUDIANTE.nombre_2 IS 'Segundo nombre del Estudiante, puede ser null.';
COMMENT ON COLUMN ESTUDIANTE.apellido_1 IS 'Primer Apellido del Estudiante.';
COMMENT ON COLUMN ESTUDIANTE.apellido_2 IS 'Segundo apellido del Estudiante, puede ser null.';
COMMENT ON COLUMN ESTUDIANTE.programa IS 'Codigo de tamano variable de 1 a 22 bytes del programa al que pertenece el Estudiante.';

create table CURSO(
	codigo number not null,
	nombre varchar2(50) not null,
	profesor_ID number not null,
	primary key (codigo),
	foreign key (profesor_ID) references PROFESOR(codigo)
);

COMMENT ON TABLE CURSO IS 'Tabla de Cursos creados por los profesores inscritos en la aplicacion.';
COMMENT ON COLUMN CURSO.codigo IS 'Codigo del Curso de tamano variable de 1 a 22 bytes, transparente al usuario, se usa para la respectiva identificacion del Curso, se autoincrementa para cada insercion garantizando valores unicos.';
COMMENT ON COLUMN CURSO.nombre IS 'Nombre del Curso.';
COMMENT ON COLUMN CURSO.profesor_ID IS 'Codigo de tamano variable de 1 a 22 bytes del profesor que registro dicho curso.';

create table INSCRIPCION(
	codigo number not null,
	estudiante_ID number not null,
	curso_cod number not null,
	primary key (codigo, estudiante_ID, curso_cod),
	foreign key (estudiante_ID) references ESTUDIANTE(codigo),
	foreign key (curso_cod) references CURSO(codigo)
);

COMMENT ON TABLE INSCRIPCION IS 'Tabla de Incripciones hechas en la aplicacion. Cada Inscripcion se refiere a la inscripcion de un estudiante en un curso previamente creado por el profesor.';
COMMENT ON COLUMN INSCRIPCION.codigo IS 'Codigo de la Inscripcion de tamano variable de 1 a 22 bytes, transparente al usuario, se usa para la respectiva identificacion de la Inscripcion, se autoincrementa para cada insercion garantizando valores unicos.';
COMMENT ON COLUMN INSCRIPCION.estudiante_ID IS 'Codigo de tamano variable de 1 a 22 bytes del estudiante que esta inscrito en un curso.';
COMMENT ON COLUMN INSCRIPCION.curso_cod IS 'Codigo de tamano variable de 1 a 22 bytes del curso al cual el estudiante quedo inscrito.';

create table EVALUACION(
	codigo number not null,
	estudiante_ID number not null,
	curso_cod number not null,
	nota number(3,2) default 0 not null check (nota >= 0.00 and nota <= 5.00),
	primary key (codigo, estudiante_ID, curso_cod),
	foreign key (estudiante_ID) references ESTUDIANTE(codigo),
	foreign key (curso_cod) references CURSO(codigo)
);

COMMENT ON TABLE EVALUACION IS 'Tabla de las Evaluaciones hechas por los estudiantes pertenecientes a cada curso, no se almacena los puntos de la evaluacion sino unicamente la nota obtenida al culminarla.';
COMMENT ON COLUMN EVALUACION.codigo IS 'Codigo de la Evaluacion de tamano variable de 1 a 22 bytes, transparente al usuario, se usa para la respectiva identificacion de la Evaluacion, se autoincrementa para cada insercion garantizando valores unicos.';
COMMENT ON COLUMN EVALUACION.estudiante_ID IS 'Codigo de tamano variable de 1 a 22 bytes del estudiante que realizo dicha evaluacion.';
COMMENT ON COLUMN EVALUACION.curso_cod IS 'Codigo de tamano variable de 1 a 22 bytes del curso al que pertenece la evaluacion.';
COMMENT ON COLUMN EVALUACION.nota IS 'Nota obtenida por el estudiante en dicha evaluacion, va desde 0.00 hasta 5.00 (un numero entero y dos decimales).';

--Comentar para ejecutar por primeravez, luego descomentar. --
DROP SEQUENCE seq_programa_codigo; 
DROP SEQUENCE seq_profesor_codigo; 
DROP SEQUENCE seq_estudiante_codigo; 
DROP SEQUENCE seq_curso_codigo; 
DROP SEQUENCE seq_inscripcion_codigo; 
DROP SEQUENCE seq_evaluacion_codigo; 

CREATE SEQUENCE seq_programa_codigo --nombre de la secuencia
START WITH 1 --la secuencia empieza por 1
INCREMENT BY 1 --se incrementa de uno en uno
NOMAXVALUE; --no tiene valor maximo

CREATE SEQUENCE seq_profesor_codigo --nombre de la secuencia
START WITH 1 --la secuencia empieza por 1
INCREMENT BY 1 --se incrementa de uno en uno
NOMAXVALUE; --no tiene valor maximo

CREATE SEQUENCE seq_estudiante_codigo --nombre de la secuencia
START WITH 1 --la secuencia empieza por 1
INCREMENT BY 1 --se incrementa de uno en uno
NOMAXVALUE; --no tiene valor maximo

CREATE SEQUENCE seq_curso_codigo --nombre de la secuencia
START WITH 1 --la secuencia empieza por 1
INCREMENT BY 1 --se incrementa de uno en uno
NOMAXVALUE; --no tiene valor maximo

CREATE SEQUENCE seq_inscripcion_codigo --nombre de la secuencia
START WITH 1 --la secuencia empieza por 1
INCREMENT BY 1 --se incrementa de uno en uno
NOMAXVALUE; --no tiene valor maximo

CREATE SEQUENCE seq_evaluacion_codigo --nombre de la secuencia
START WITH 1 --la secuencia empieza por 1
INCREMENT BY 1 --se incrementa de uno en uno
NOMAXVALUE; --no tiene valor maximo


--Triggers--
CREATE OR REPLACE TRIGGER trig_programa_seq
  BEFORE INSERT ON PROGRAMA
  FOR EACH ROW
  BEGIN
    SELECT seq_programa_codigo.nextval INTO :new.codigo FROM dual;
  END;
/

CREATE OR REPLACE TRIGGER trig_profesor_seq
  BEFORE INSERT ON PROFESOR
  FOR EACH ROW
  BEGIN
    SELECT seq_profesor_codigo.nextval INTO :new.codigo FROM dual;
  END;
/

CREATE OR REPLACE TRIGGER trig_estudiante_seq
  BEFORE INSERT ON ESTUDIANTE
  FOR EACH ROW
  BEGIN
    SELECT seq_estudiante_codigo.nextval INTO :new.codigo FROM dual;
  END;
/

CREATE OR REPLACE TRIGGER trig_curso_seq
  BEFORE INSERT ON CURSO
  FOR EACH ROW
  BEGIN
    SELECT seq_curso_codigo.nextval INTO :new.codigo FROM dual;
  END;
/

CREATE OR REPLACE TRIGGER trig_inscripcion_seq
  BEFORE INSERT ON INSCRIPCION
  FOR EACH ROW
  BEGIN
    SELECT seq_inscripcion_codigo.nextval INTO :new.codigo FROM dual;
  END;
/

CREATE OR REPLACE TRIGGER trig_evaluacion_seq
  BEFORE INSERT ON EVALUACION
  FOR EACH ROW
  BEGIN
    SELECT seq_evaluacion_codigo.nextval INTO :new.codigo FROM dual;
  END;
/



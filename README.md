# Proyecto Final #

Proyecto web que hace uso de IaaS (Infraestructure as a Service), arquitectura de 3 capas, además de
ORM, patrón Singleton y DAO, implementación nivel de conformidad A de accesibilidad, manual
técnico y de usuario, minificar Javascript, web services, GUI response.

# Entrega 1 #

Matriz de la asignación de responsabilidades (RACI), Formato IEEE 830, diagrama de casos de uso
(diagrama y la ficha, disponible en curso virtual), Diagrama de clases, estimación de tiempo, costo y
esfuerzo, planificación completa (costo, tiempo, responsable) en una herramienta web.

# Entrega 2 #

Informe de avance, plan de gestión del riesgo, plan de pruebas, diagrama de secuencia, Diccionario de
datos y de clases, DER (diagrama entidad relación), MER (Modelo entidad relación), Interfaces
funcionales (validación, SEO favicon, test para identificar a humanos distinto de captcha o recaptcha)


# Evaluación #
* Proyecto final 20%
* Entrega 1 10%
* Entrega 2 10%
* Quiz 20%
* Exposición 10%
* Salida 10%
* Libro (5 controles) 10%
vParcial 10%